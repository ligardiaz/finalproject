## Pengantar

Alhamdulillah, tugas ini telah terselesaikan. Saya mengucapkan terima kasih yang banyak kepada Bapak Nursyafriady untuk bimbingannya dan team PKS Digital School.
Tugas ini dimulai dikerjakan pada hari Senin 27 November dan selesai pada hari Minggu 2 Desember 2023.

## ERD

Berikut adalah tampilan ERD yang dipakai untuk tugas ini
<p align="center"><img src="https://s6.imgcdn.dev/VTtlL.png" width="400"></a></p>

## Genre

Di dalam tab Genre, middleware auth diberikan untuk seluruh halaman Genre. Berikut tampilan genre
<p align="center"><img src="https://s6.imgcdn.dev/VTfMu.jpg" width="400"></a></p>


## Film

Di dalam tab Film, middleware auth diberikan untuk fungsi edit. Fitur Delete berada di dalam halaman edit tersebut. Di dalam halaman show ada fitur tampilan review dan tambah review.
<p align="center"><img src="https://s6.imgcdn.dev/VT9Bq.jpg" width="400"></a></p>
<p align="center">tampilan index Film</p>

<p align="center"><img src="https://s6.imgcdn.dev/VTZNN.jpg" width="400"></a></p>
<p align="center">tampilan edit Film dengan dropdown Genre</p>

<p align="center"><img src="https://s6.imgcdn.dev/VTRbB.jpg" width="400"></a></p>
<p align="center">fitur show film, list review & tambah review</p>


<br>

#### Kesulitan yang sebelumnya dihadapi

- file poster yang tidak muncul setelah edit
- konten review yang tidak muncul setelah ditambahkan

